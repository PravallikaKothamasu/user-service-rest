package com.epam.userservice.controller;

import com.epam.userservice.model.User;
import com.epam.userservice.model.UserDTO;
import com.epam.userservice.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    @ApiOperation(value = "Returns All Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieves all users")
    })
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("{userId}")
    @ApiOperation(value = "Returns user with given userId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "User not found with given id")
    })
    public ResponseEntity<User> getUserById(@ApiParam(value = "User Id for which the user will be retrieved",
            required = true) @PathVariable int userId) {
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @PostMapping
    @ApiOperation(value = "Adds a user")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created new user")
    })
    public ResponseEntity<User> addUser(@ApiParam(value = "User that is to be added",
			required = true) @RequestBody UserDTO userDTO) {
        User addedUser = userService.addUser(userDTO);
        final java.net.URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{bookId}")
                .buildAndExpand(addedUser.getUserId()).toUri();
        return ResponseEntity.created(uri).body(addedUser);
    }

    @PutMapping("{userId}")
    @ApiOperation(value = "Updates a user having given userId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated user with given userId"),
            @ApiResponse(code = 404, message = "User not found with given user id")
    })
    public ResponseEntity<User> updateUser(@ApiParam(value = "User ID for which the user will be updated",
			required = true) @PathVariable int userId, @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.updateUser(userId, userDTO));
    }

    @DeleteMapping("{userId}")
    @ApiOperation(value = "Deletes a user with given userId")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted user with given userId"),
            @ApiResponse(code = 404, message = "User not found with given user id")
    })
    public ResponseEntity<Void> deleteUSer(@ApiParam(value = "User ID for which the user will be deleted",
			required = true) @PathVariable int userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
