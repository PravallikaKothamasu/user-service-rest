FROM openjdk:8-jre
COPY target /userservice
WORKDIR /userservice
ENTRYPOINT ["java","-jar","userservicerest-0.0.1-SNAPSHOT.jar"]